// src/models/user.ts

import { Product } from "./Product";

export class User {
    id: number;
    name: string;
    profileImg: string;
    travels: Product[];
}
