// src/models/product.ts

export class Product {
    id: number;
    name: string;
    price: number;
    gridImg: string;
    mainImg: string;
    desc: string;
}