import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  private id: number;
  public product: Product;

  constructor(private route: ActivatedRoute,
              private productService: ProductService) {

   }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('pid');
    this.product = this.productService.getProductById(this.id);
  }

}
