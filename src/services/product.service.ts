import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { PRODUCTS } from '../data/mock-products';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  getProducts(): Product[]{
    return PRODUCTS;
  }

  getProductById(id: number): Product{

    for (let prod of PRODUCTS)
    {
      if (prod.id == id) return prod;
    }
  }
}
